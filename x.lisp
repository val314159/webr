(load "macros")
qload "woo" "clack" "f-underscore" "cl-interpol"
cl-interpol:enable-interpol-syntax
ff #?"blah\nqwew\n${(L 4 5)}"
use-package keyword:f-underscore
defmacro q(x) `',x
;;defmacro qq(x) ``,x
defmacro F1(&body body) `(lambda(_1         ),@body)
defmacro F2(&body body) `(lambda(_1 _2      ),@body)
defmacro F3(&body body) `(lambda(_1 _2 _3   ),@body)
defmacro F4(&body body) `(lambda(_1 _2 _3 _4),@body)
defvar xyz 999
defun read-input (env) >
  concatenate'string >
    iter >
      for i below > getf env keyword:content-length
      collect > code-char >
        flexi-streams::read-byte >
          getf env keyword:raw-body

clack:clackup >
  lambda (env) >
    L >
      v 200
      L keyword:content-type "text/plain"
      L >
        v "Hello, World:"
        read-input env
        v "
"
  v keyword:server
  v keyword:woo
  v keyword:use-default-middlewares
  v nil
sleep 6000000
